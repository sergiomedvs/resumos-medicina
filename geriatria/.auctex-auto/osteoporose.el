(TeX-add-style-hook
 "osteoporose"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref")
   (LaTeX-add-labels
    "sec:orgb33301b"
    "sec:org1f1b8fc"
    "sec:org3aefd3c"
    "sec:orgc32fe5e"
    "sec:orgdb1908d"
    "sec:orgac7ec0f"
    "sec:org3205c5b"
    "sec:orge4bd4a4"
    "sec:org08969c8"
    "sec:org979b56f"
    "sec:orgfff3c28"
    "sec:org8f61ec7"
    "sec:orgd395bd2"
    "sec:orgc10acf4"
    "sec:org31a8c36"
    "sec:org9c28d5d"
    "sec:orgd70fb0b"
    "sec:org787fea0"
    "sec:org4a2ab31"
    "sec:orgb07c67e"
    "sec:org30d8111"
    "sec:org2416ab9"
    "sec:org62d5187"
    "sec:org8a36240"
    "sec:org23edc9a"
    "sec:orgfc0339d"
    "sec:org7572b06"
    "sec:orgf7bb02d"
    "sec:orgb6deda2"
    "sec:orgaf87b59"
    "sec:org7218081"
    "sec:org94ba139"
    "sec:org1bfe3d5"
    "sec:org70c564c"
    "sec:org1bee795"
    "sec:org9736133"
    "sec:orgc19dab1"
    "sec:org114d6c7"
    "sec:org8efabe9"
    "sec:orgd5e642b"
    "sec:orgb87bd92"
    "sec:org3fc17af"
    "sec:org21b4be8"
    "sec:orgba358f7"
    "sec:org211e406"
    "sec:org326ecd3"
    "sec:orge27c053"
    "sec:org2053b3b"
    "sec:orgcf479fb"
    "sec:org5367916"
    "sec:org9cddc6f"))
 :latex)

