(TeX-add-style-hook
 "control-de-sintomas"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref")
   (LaTeX-add-labels
    "sec:org6413cbc"
    "sec:org33c93e3"
    "sec:orgd1ffb14"
    "sec:orgd0809ad"
    "sec:org8e97f67"
    "sec:org7da5d74"
    "sec:org18c917f"
    "sec:orgc1bdcbf"
    "sec:org00d261a"
    "sec:org63cddfb"
    "sec:org124c23b"
    "sec:orgbe02d75"
    "sec:org02e3ef7"
    "sec:org59b2712"
    "sec:orgb3f2011"
    "sec:org7a11037"
    "sec:org1cc92d1"
    "sec:org123be22"
    "sec:orgdd5bed5"
    "sec:orgced0452"))
 :latex)

