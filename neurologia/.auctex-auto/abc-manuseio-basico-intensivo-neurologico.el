(TeX-add-style-hook
 "abc-manuseio-basico-intensivo-neurologico"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref")
   (LaTeX-add-labels
    "sec:orgcdba420"
    "sec:org4aaaa13"
    "sec:org8090d43"
    "sec:org8b014d1"
    "sec:org40531d5"
    "sec:org6e17730"
    "sec:org7636717"
    "sec:org04d7af4"
    "sec:org8ed4c82"
    "sec:org50bcd16"
    "sec:org7a88400"
    "sec:org11a0412"
    "sec:org0d105c7"
    "sec:org2ea38fe"
    "sec:org681b423"
    "sec:org33008ad"
    "sec:orgf312e0a"
    "sec:orgeffcd7f"
    "sec:orge9d783b"
    "sec:org66b1228"
    "sec:org9cb41d1"
    "sec:org508ae8b"
    "sec:org200b5a0"
    "sec:org78784eb"
    "sec:orgbfff32f"
    "sec:org9c376ae"
    "sec:org7ba7e70"
    "sec:orgfce603d"
    "sec:org96d67c3"
    "sec:org438f487"
    "sec:org1cc848f"
    "sec:orgfabcdf5"
    "sec:org109cc85"
    "sec:org143cd3b"
    "sec:orgead6f09"
    "sec:orgfc80dbf"
    "sec:org7d62ec9"
    "sec:org8f74b2c"
    "sec:org5d6c919"
    "sec:org110ba4f"
    "sec:org18a36f2"
    "sec:orge17b1b0"
    "sec:org0caaf76"
    "sec:org20a04e8"
    "sec:org14beda3"
    "sec:org1b3ca6d"
    "sec:org12054a2"
    "sec:orgd571044"
    "sec:org53d4451"
    "sec:org9e3591f"
    "sec:org5386f64"
    "sec:org28d0594"
    "sec:orgd8d1812"
    "sec:org7c6cbbd"
    "sec:org22f87b7"
    "sec:org669a01a"
    "sec:orgd149a28"
    "sec:org29b232e"
    "sec:org748dfd2"
    "sec:org9cd3676"))
 :latex)

