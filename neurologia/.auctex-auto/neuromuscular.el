(TeX-add-style-hook
 "neuromuscular"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref")
   (LaTeX-add-labels
    "sec:org3c3bf24"
    "sec:org137fd37"
    "sec:orgacf9f26"
    "sec:orgf1e4f67"
    "sec:org0f69491"
    "sec:org8b3de66"
    "sec:org3776026"
    "sec:orgd1a90f8"
    "sec:org2ed25c9"
    "sec:orgd293c55"
    "sec:orgbe4ac16"
    "sec:orga4fe111"
    "sec:org89619d6"
    "sec:org3894da5"
    "sec:org2a0cc65"
    "sec:org02bd319"
    "sec:org6b8a329"
    "sec:org5422a9f"
    "sec:org8fd563c"
    "sec:org346a645"
    "sec:org77bae18"
    "sec:org68d2abf"
    "sec:org99e096c"
    "sec:org623aa15"
    "sec:orgc8856f1"
    "sec:org476493a"
    "sec:org27371b5"
    "sec:org2f562a1"
    "sec:orgc037daf"
    "sec:orgeca9e6e"
    "sec:org0ed641a"
    "sec:org135acab"
    "sec:org12fdd59"
    "sec:org5edaa71"
    "sec:org14fb521"
    "sec:org77755e2"
    "sec:orge2d9b20"))
 :latex)

