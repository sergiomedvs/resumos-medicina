#+TITLE: Abc Manuseio Basico Intensivo Neurologico
#+OPTIONS: toc:nil
* Introdução
** Antecipação
** Diagnóstico precoce
** ATLS e Fundamental critical care support (FCCS)
** Profilaxia
- TVP
- Ulceras de pressão
- Hemorragia digestiva gástrica ou duodenal por estress
- Sondas e cateteres
- Desidratação, hipovolemia e hipotensão
- Desnutrição
- Disturbios hidroeletrolíticos e acidobasico
* Atendimento inicial
- Identificar e reverter alterações ameaçadoras de vida imediatas
** Exame primário (ABCDE)
*** A (Airway) - Manuntenção de vias aéreas
****
** Repetir ABCDE
** Monitorar
** Exame secundário
*** Exame clínico detalhado
*** Conversa com familiares
- Diagnóstico prévios
- Drogas utilizadas
- Etc
** Exames complementares
- Diagnóstico e monitorização
- Toda a transferência exige reavaliação -> Evitar fatores de lesão secundária
*** Laboratoriais
*** Imagem
**** TC de crânio sem contraste
- Básico neurológico
* Controle de sangramento
** Paciente com doença cerebrovascular aguda
- Disturbios de coagulação (10% dos casos de hemorragia intracraniana)
** Se uso de anticoagulante em menos de duas horas
- considerar lavagem gástrica com carvão ativado
** Reversão imediata da anticoagulação se INR>1,4
- Exceto se trombose venosa central
** Trauma e pós-operatório neurocirúrgico sempre se INR>1,4
** AVC isquêmico INR >1,7 contraindica a trombólise
** Reversão efeito cumarínico
*** Vitamina K 10mg EV
- Pode repetir em 24h
- Início lento (24h), mas duradouro
*** Plasma fresco
- Rápido
- Volume elevado (sobrecarga de volume)
  + 15-20mL/Kg
- Risco de reação transfusionais
*** Concentrado protrombínico
- Imediato
  + Sangramento ativo
  + Para realização de procedimentos cirugicos
- Menor volume
  + 25-50UI/kg
- Maior custo
** Reversão dos efeitos da heparina e derivados fracionados
*** Protamina EV
**** Heparinas não fracionadas (HNF)
- 1mg para cada 100UI de heparina (Últimas 2-3h)
- Maximo de 20mg/min (Não pode fazer mais 50mg em 10min)
**** Heparinas de baixo peso molecular (HBPM)
- 1mg para cada 1mg (Ultimas 8h)
- Maximo de 20mg/min (Não pode fazer mais 50mg em 10min)
** Reversão dos novos anticoagulantes orais
*** Dabigatran
**** Idarucizumab
- 2,5g EV lenta (5min) duas vezes com intervalo de 15 minutos
**** Complexo protrombínico
- 50UI/kg
*** Inibidores de Xa
**** Complexo protrombínico
- 25-50UI/kg
** Terapia com concentrado de plaquetas
*** Reações transfusionais
- Alto risco
**** TRALI (transfusion-related acute lung injury)
**** Trombose
**** CIVD
*** Indicação
**** Somente Hemorragia intracraniana com uso de antiplaquetários com indicação cirugica
**** Profilatico
***** <5.000-10.000 -> Sempre
- Risco de sangramento espontâneo SNC
***** <20.000 -> Considerar risco de sangramento
***** <50.000-100.000
- Sangramento ativo
- Planejamento de procedimento cirugico
*** Contraindicação
**** Púrpura trombocitopenica trombotica (PTT)
**** Trombocitopenia induzida por heparina
**** Púrpura trombocitopenica idiopática (PTI)
*** Desmopressina (DDAVP)
**** Preferida
**** Pode ser associada
**** 0,4 mcg/kg
* Trombose Venosa Profunda
** Complicações
- Tromboembolismo pulmonar (TEP)
- Síndrome pós-TVP
** Profilaxia
*** Não farmacológica
