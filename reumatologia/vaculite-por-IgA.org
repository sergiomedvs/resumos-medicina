#+TITLE: Vaculite Por Ig A
* Definição
- Purpura Anafilactóide
- Púrpura de Henoch-Scholein
- Vasculite sistêmica de pequenos vasos
  + Deposição de igA1
* Epidemiologia
- Autolimitado
- Mais comum da infância
  + Menor de 10 anos
  + 2 meninos :1 menina
- Sazonal
  + Trigger
    - Infecciosa
    - Drogas
    - Pos vacinal
    - Picada de insetos
    - Frio
- Adultos com pior prognóstico
  + Renal
  + TGI
* Fisiopatlogia
** 50-70% Elevação de IgA sérico
*** Erro de glicosilação
*** Autoagregação -> imunocomplexos
** Depósito em parede de vasos
*** IgA
*** C3
** Vasculite leucocistoclástica
** Infiltração neutrofílica
* Clínica
** Tétrade
*** Purpura palpável
**** Não trombocitopênica
**** Nádegas e MMII
**** Simétrica
*** Artralgia/artrite
**** Oligoartrite
- MMII
*** Sintomas gastrointestinais
**** Dor abdominal
**** Náuseas e vômitos
**** Melena
**** Hematêmese
**** Intssucepção
*** Comprometimento renal
**** Glomerulonefrite
- Depósito de IgA (Imunofluorescência)
***** Mesangial proliferativa
***** Crescênctica
* Diagnóstico
- Clínica + Anatomopatológico
- Laboratorial serico auxilia mas não exclui
* Tratamento
** Autolimitada
- Suporte
** TGI ou Renal
- Glicocorticóide
** Imunossupressores ajudam a retirar corticóide
